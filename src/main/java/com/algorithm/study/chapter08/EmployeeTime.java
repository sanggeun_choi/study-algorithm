package com.algorithm.study.chapter08;

import lombok.Data;

/**
 * @author sanggeun.choi
 */
@Data
public class EmployeeTime {
	private String onTime;
	private String offTime;

	public EmployeeTime(String onTime, String offTime) {
		this.onTime = onTime;
		this.offTime = offTime;
	}
}
