package com.algorithm.study.chapter08;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sanggeun.choi
 */
public class EmployeeChecker {
	private static final List<EmployeeTime> EMPLOYEE_TIME_LIST = new ArrayList<EmployeeTime>();
	static {
		EMPLOYEE_TIME_LIST.add(new EmployeeTime("09:12:23", "11:14:35"));
		EMPLOYEE_TIME_LIST.add(new EmployeeTime("10:34:01", "13:23:40"));
		EMPLOYEE_TIME_LIST.add(new EmployeeTime("10:34:31", "11:20:10"));
	}

	public static int checkEmployeeCount(String time) {
		int employeeCount = 0;

		for (EmployeeTime employeeTime : EMPLOYEE_TIME_LIST) {
			if (time.compareTo(employeeTime.getOnTime()) >= 0 && time.compareTo(employeeTime.getOffTime()) <= 0) {
				employeeCount++;
			}
		}

		return employeeCount;
	}

	public static void main(String[] args) {
		int employeeCount = EmployeeChecker.checkEmployeeCount("11:05:20");
		System.out.println(employeeCount);
	}
}
