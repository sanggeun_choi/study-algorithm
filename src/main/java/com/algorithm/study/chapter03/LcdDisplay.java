package com.algorithm.study.chapter03;

import java.util.Arrays;
import java.util.List;

import com.algorithm.study.chapter03.model.LcdDisplayParameter;

/**
 * @author sanggeun.choi
 */
public class LcdDisplay {

	public String prepareDisplayNumbers(List<LcdDisplayParameter> lcdDisplayParameterList) {
		String result = "";

		for (LcdDisplayParameter lcdDisplayParameter : lcdDisplayParameterList) {
			result += prepareDisplayNumber(lcdDisplayParameter);
			result += "\n\n";
		}

		return result;
	}

	private String prepareDisplayNumber(LcdDisplayParameter lcdDisplayParameter) {
		int s = lcdDisplayParameter.getS();
		int n = lcdDisplayParameter.getN();

		return null;
	}

	public static void main(String[] args) {
		List<LcdDisplayParameter> lcdDisplayParameterList = Arrays.asList(new LcdDisplayParameter(2, 12345), new LcdDisplayParameter(3, 67890));
		LcdDisplay lcdDisplay = new LcdDisplay();

		String displayNumbers = lcdDisplay.prepareDisplayNumbers(lcdDisplayParameterList);

		System.out.println(displayNumbers);
	}
}
