package com.algorithm.study.chapter03;

/**
 * @author sanggeun.choi
 */
public class GoogleTest {
	public int countEight(int startNumber, int endNumber) {
		int count = 0;

		for (int number = startNumber; number <= endNumber; number++) {
			count += countEight(number);
		}

		return count;
	}

	private int countEight(int number) {
		String numberText = "" + number;
		int count = 0;

		for (char digitCharacter : numberText.toCharArray()) {
			int digit = digitCharacter - '0';

			if (digit == 8) {
				count++;
			}
		}

		return count;
	}

	public static void main(String[] args) {
		GoogleTest googleTest = new GoogleTest();

		int count = googleTest.countEight(1, 10000);

		System.out.println(count);
	}
}
