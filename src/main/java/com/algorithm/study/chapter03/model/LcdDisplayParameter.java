package com.algorithm.study.chapter03.model;

/**
 * @author sanggeun.choi
 */
public class LcdDisplayParameter {

	private int s;
	private int n;

	public LcdDisplayParameter(int s, int n) {
		this.s = s;
		this.n = n;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
}
