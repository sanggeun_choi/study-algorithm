package com.algorithm.study.chapter01;

import com.algorithm.study.utils.IntegerListUtils;
import com.algorithm.study.utils.SwapUtils;

import java.util.List;

/**
 * @author sanggeun.choi
 */
public class QuickSorter {
	public static <E extends Comparable<? super E>> void sort(List<E> list) {
		rInPlaceQuickSort(list, 0, list.size()-1);
	}

	private static <E extends Comparable<? super E>> void rInPlaceQuickSort(List<E> list, int startIndex, int endIndex) {
		if(startIndex >= endIndex) {
			return;
		}

		int pivotIndex = startIndex;

		pivotIndex = inPlacePartition(list, startIndex, endIndex, pivotIndex);

		int leftEndIndex = pivotIndex - 1;
		int rightStartIndex = pivotIndex + 1;

		rInPlaceQuickSort(list, startIndex, leftEndIndex);
		rInPlaceQuickSort(list, rightStartIndex, endIndex);
	}

	private static <E extends Comparable<? super E>> int inPlacePartition(List<E> list, int startIndex, int endIndex, int pivotIndex) {
		// 가장 왼쪽 인덱스의 데이터가 피봇이 됨
		E pivot = list.get(pivotIndex);

		// 피봇 데이터값이 가장 크다고 간주하고 맨 뒤로 보냄
		SwapUtils.swap(list, pivotIndex, endIndex);

		// 맨 뒤에있는 피봇 데이터의 왼쪽 그룹을 가지고 정렬을 시작하기 위해 인덱스 설정
		int left = startIndex;
		int right = endIndex - 1;

		// 정렬 시작
		while(left <= right) {
			// 왼쪽 인덱스가 오른쪽 인덱스보다 왼쪽에 있고, 왼쪽 인덱스의 원소값이 피벗보다 큰 값이 나올 때 까지 오른쪽으로 이동
			while((left <= right) && pivot.compareTo(list.get(left)) >= 0) {
				left++;
			}

			// 왼쪽 인덱스가 오른쪽 인덱스보다 왼쪽에 있고, 오른쪽 인덱스의 원소값이 피벗보다 작은 값이 나올 때 까지 왼쪽으로 이동
			while((left <= right) && pivot.compareTo(list.get(right)) <= 0) {
				right--;
			}

			// 피벗보다 큰 값을 가진 왼쪽 인덱스 원소값과, 피벗보다 작은 값을 가진 오른쪽 인덱스 원소값을 교환
			if(left < right) {
				SwapUtils.swap(list, left, right);
			}
		}

		// 왼쪽 인덱스는 제일 큰 원소를 찾을 때 까지 이동했을 것이기 때문에
		// 왼쪽 인덱스의 원소값과 맨 오른쪽 인덱스의 원소값을 교환
		SwapUtils.swap(list, left, endIndex);

		return left;
	}

	public static void main(String[] args) {
		List<Integer> randomList = IntegerListUtils.makeRandomIntegerList(10000);

		IntegerListUtils.printRandomIntegerList(randomList);

		long start = System.currentTimeMillis();

		QuickSorter.sort(randomList);

		long end = System.currentTimeMillis();

		IntegerListUtils.printRandomIntegerList(randomList);

		System.out.println("정렬소요시간 : " + (end - start));
	}
}