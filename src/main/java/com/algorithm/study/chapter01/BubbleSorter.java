package com.algorithm.study.chapter01;

import com.algorithm.study.utils.IntegerListUtils;
import com.algorithm.study.utils.SwapUtils;

import java.util.List;

/**
 * @author sanggeun.choi
 */
public class BubbleSorter {
	public static <E extends Comparable<? super E>> void sort(List<E> list) {
		// 맨 뒤에 있는 원소가 가장 큰 값을 가짐
		for (int i=list.size()-1; i>0; i--) {
			// 맨 뒤 원소의 앞 원소까지만 비교
			for (int j=0; j<i; j++) {
				// j의 원소가 j의 뒤에 있는 원소보다 크면 위치 바꿈
				if (list.get(j).compareTo(list.get(j+1)) > 0) {
					SwapUtils.swap(list, j, j+1);
				}
			}
		}
	}

	public static void main(String[] args) {
		List<Integer> randomList = IntegerListUtils.makeRandomIntegerList(10000);

		IntegerListUtils.printRandomIntegerList(randomList);

		long start = System.currentTimeMillis();

		BubbleSorter.sort(randomList);

		long end = System.currentTimeMillis();

		IntegerListUtils.printRandomIntegerList(randomList);

		System.out.println("정렬소요시간 : " + (end - start));
	}
}
