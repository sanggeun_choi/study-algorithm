package com.algorithm.study.chapter01;

import java.util.List;

import com.algorithm.study.utils.IntegerListUtils;
import com.algorithm.study.utils.SwapUtils;

/**
 * @author sanggeun.choi
 */
public class SelectionSorter {
    public static <E extends Comparable<? super E>> void sort(List<E> list) {
        int minIndex = 0;

        for (int i=0; i<list.size()-1; i++) {
            minIndex = i;

            for (int j=i+1; j<list.size(); j++) {
                if (list.get(minIndex).compareTo(list.get(j)) > 0) {
                    minIndex = j;
                }
            }

            if (i != minIndex) {
                SwapUtils.swap(list, i, minIndex);
            }
        }
    }

    public static void main(String[] args) {
        List<Integer> randomList = IntegerListUtils.makeRandomIntegerList(10000);

        IntegerListUtils.printRandomIntegerList(randomList);

        long start = System.currentTimeMillis();

        SelectionSorter.sort(randomList);

        long end = System.currentTimeMillis();

        IntegerListUtils.printRandomIntegerList(randomList);

        System.out.println("정렬소요시간 : " + (end - start));
    }
}
