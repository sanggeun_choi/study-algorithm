package com.algorithm.study.chapter01;

import com.algorithm.study.utils.IntegerListUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sanggeun.choi
 */
public class MergeSorter {

    public static <E extends Comparable<? super E>> void sort(List<E> list) {
        rMergeSort(list, 0, list.size() - 1);
    }

    public static <E extends Comparable<? super E>> void rMergeSort(List<E> list, int startIndex, int endIndex) {
        if (startIndex >= endIndex) {
            return;
        }

        int midIndex = (startIndex + endIndex) / 2;

        rMergeSort(list, startIndex, midIndex);
        rMergeSort(list, midIndex + 1, endIndex);

        int leftIndex = startIndex;
        int rightIndex = midIndex + 1;
        List<E> tempList = new ArrayList<E>();

        while (leftIndex <= midIndex && rightIndex <= endIndex) {
            if (list.get(leftIndex).compareTo(list.get(rightIndex)) < 0) {
                tempList.add(list.get(leftIndex++));
            } else {
                tempList.add(list.get(rightIndex++));
            }
        }

        while (leftIndex <= midIndex) {
            tempList.add(list.get(leftIndex++));
        }

        while (rightIndex <= endIndex) {
            tempList.add(list.get(rightIndex++));
        }

        for (int i=startIndex; i<=endIndex; i++) {
            list.set(i, tempList.remove(0));
        }
    }

    public static void main(String[] args) {
        List<Integer> randomList = IntegerListUtils.makeRandomIntegerList(6);

        IntegerListUtils.printRandomIntegerList(randomList);

        long start = System.currentTimeMillis();

        MergeSorter.sort(randomList);

        long end = System.currentTimeMillis();

        IntegerListUtils.printRandomIntegerList(randomList);

        System.out.println("정렬소요시간 : " + (end - start));
    }
}
