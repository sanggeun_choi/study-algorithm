package com.algorithm.study.chapter01;

import com.algorithm.study.utils.IntegerListUtils;
import com.algorithm.study.utils.SwapUtils;

import java.util.List;

/**
 * @author sanggeun.choi
 */
public class InsertionSorter {

    public static <E extends Comparable<? super E>> void sort(List<E> list) {
        for (int i=0; i<list.size()-1; i++) {
            for (int j=i+1; j>0; j--) {
                if (list.get(j - 1).compareTo(list.get(j)) > 0) {
                    SwapUtils.swap(list, j - 1, j);
                }
            }
        }
    }

    public static void main(String[] args) {
        List<Integer> randomList = IntegerListUtils.makeRandomIntegerList(10000);

        IntegerListUtils.printRandomIntegerList(randomList);

        long start = System.currentTimeMillis();

        InsertionSorter.sort(randomList);

        long end = System.currentTimeMillis();

        IntegerListUtils.printRandomIntegerList(randomList);

        System.out.println("정렬소요시간 : " + (end - start));
    }
}
