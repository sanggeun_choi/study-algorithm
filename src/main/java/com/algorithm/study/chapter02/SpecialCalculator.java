package com.algorithm.study.chapter02;

/**
 * @author sanggeun.choi
 */
public class SpecialCalculator {

	public static int add(int number) {
		if (number == 1) {
			return 1;
		}

		return number + add(number - 1);
	}

	public static void main(String[] args) {
		int result = SpecialCalculator.add(10);

		System.out.println(result);
	}

}
