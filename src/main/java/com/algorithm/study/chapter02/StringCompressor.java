package com.algorithm.study.chapter02;

/**
 * @author sanggeun.choi
 */
public class StringCompressor {

	public static String compress(String text) {
		char buffer = text.charAt(0);
		int count = 1;
		String result = "";

		for (int i=1; i<text.length(); i++) {
			char c = text.charAt(i);

			if (c != buffer) {
				result += ("" + buffer) + count;

				buffer = c;
				count = 1;
			} else {
				count++;
			}
		}

		result += ("" + buffer) + count;

		return result;
	}

	public static void main(String[] args) {
		String text = "aabccccaa";

		String compressedText = StringCompressor.compress(text);

		System.out.println(compressedText);
	}

}
