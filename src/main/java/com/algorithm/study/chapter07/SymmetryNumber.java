package com.algorithm.study.chapter07;

/**
 * Created by john on 2017-01-30.
 */
public class SymmetryNumber {
    public static void main(String[] args) {
        int symmetryNumber1 = searchSymmetryNumber(101);
        int symmetryNumber2 = searchSymmetryNumber(210);
        System.out.println(symmetryNumber1 == 929);
        System.out.println(symmetryNumber2 == 11111);
    }

    public static int searchSymmetryNumber(int index) {
        int number = 0;
        int i = 0;

        while (true) {
            if (isEqualNumber(number)) {
                i++;
            }
            if (i > index) {
                break;
            }
            number++;
        }

        return number;
    }

    private static boolean isEqualNumber(int number) {
        String number1 = "" + number;
        String number2 = "";

        for (int i=number1.length()-1; i>=0; i--) {
            char c = number1.charAt(i);
            number2 += c;
        }

        return number1.equals(number2);
    }
}
