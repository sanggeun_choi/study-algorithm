package com.algorithm.study.chapter07;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2017-01-30.
 */
public class SymmetryNumber2 {
    public static void main(String[] args) {
        List<Integer> equalNumberList = new ArrayList<Integer>();
        for (int i=0; i<1000000; i++) {
            if (isEqualNumber(i)) {
                equalNumberList.add(i);
            }
        }

        for (int i=0; i<equalNumberList.size(); i++) {
            System.out.println(String.format("%d => %d", i, equalNumberList.get(i)));
        }
    }

    private static boolean isEqualNumber(int number) {
        String number1 = "" + number;
        String number2 = "";

        for (int i=number1.length()-1; i>=0; i--) {
            char c = number1.charAt(i);
            number2 += c;
        }

        return number1.equals(number2);
    }
}
