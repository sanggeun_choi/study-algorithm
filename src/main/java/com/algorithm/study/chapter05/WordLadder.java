package com.algorithm.study.chapter05;

import java.util.*;

/**
 * Created by john on 2017-01-20.
 */
public class WordLadder {
    private static final String[] DICTIONARY_WORDS = {"hot", "dot", "dog", "lot", "log"};
    private List<WordNode> dictionaryWordNodeList = new ArrayList<WordNode>();
    private List<WordNode> startWordNodeList = new ArrayList<WordNode>();
    private List<WordNode> endWordNodeList = new ArrayList<WordNode>();

    public List<Path> searchShortestPath(String startWordName, String endWordName) {
        makeDictionaryNode();
        placeStartEndWordNode(startWordName, endWordName);

        Queue<WordNode> nodeQueue = new LinkedList<WordNode>();
        List<Path> pathList = new ArrayList<Path>();

        for (WordNode startWordNode : startWordNodeList) {
            nodeQueue.add(startWordNode);
        }

        while (!nodeQueue.isEmpty()) {
            WordNode visitNode = nodeQueue.remove();
            visitNode.setVisited(true);
            visitNode.addPath(visitNode.getName());

            for (WordNode adjoinNode : visitNode.getAdjoinWordNodeSet()) {
                if (!adjoinNode.isVisited() && !nodeQueue.contains(adjoinNode)) {
                    nodeQueue.add(adjoinNode);
                    adjoinNode.copyPathList(visitNode.getPathList());
                }

                if (visitNode.getPathSize() + 1 < adjoinNode.getPathSize()) {
                    adjoinNode.copyPathList(visitNode.getPathList());
                } else if (visitNode.getPathSize() + 1 == adjoinNode.getPathSize()) {
                    adjoinNode.addPathList(visitNode.getPathList());
                }
            }
        }

        for (WordNode endWordNode : endWordNodeList) {
            for (Path path : endWordNode.getPathList()) {
                pathList.add(path);
            }
        }

        return pathList;
    }

    private boolean isOneCharacterDifferent(String wordName1, String wordName2) {
        int differentCharacterCount = 0;

        for (int i=0; i<wordName1.length(); i++) {
            if (wordName1.charAt(i) != wordName2.charAt(i)) {
                differentCharacterCount++;
            }
        }

        return differentCharacterCount == 1;
    }

    private void makeDictionaryNode() {
        for (String word : DICTIONARY_WORDS) {
            WordNode wordNode = new WordNode(word);

            for (WordNode tempNode : dictionaryWordNodeList) {
                if (isOneCharacterDifferent(wordNode.getName(), tempNode.getName())) {
                    tempNode.addAdjoinWordNode(wordNode);
                    wordNode.addAdjoinWordNode(tempNode);
                }
            }

            dictionaryWordNodeList.add(wordNode);
        }
    }

    private void placeStartEndWordNode(String startWordName, String endWordName) {
        for (WordNode tempNode : dictionaryWordNodeList) {
            WordNode startWordNode = new WordNode(startWordName);
            WordNode endWordNode = new WordNode(endWordName);

            if (isOneCharacterDifferent(startWordNode.getName(), tempNode.getName())) {
                tempNode.addAdjoinWordNode(startWordNode);
                startWordNode.addAdjoinWordNode(tempNode);
                startWordNodeList.add(startWordNode);
            }

            if (isOneCharacterDifferent(endWordNode.getName(), tempNode.getName())) {
                tempNode.addAdjoinWordNode(endWordNode);
                endWordNode.addAdjoinWordNode(tempNode);
                endWordNodeList.add(endWordNode);
            }
        }
    }
}
