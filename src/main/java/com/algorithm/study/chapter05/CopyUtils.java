package com.algorithm.study.chapter05;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2017-01-20.
 */
public class CopyUtils {
    public static List<Path> copy(List<Path> pathList) {
        List<Path> newPathList = new ArrayList<Path>();

        for (Path path : pathList) {
            Path newPath = new Path();
            List<String> wordList = new ArrayList<String>();

            for (String word : path.getWordList()) {
                wordList.add(new String(word));
            }

            newPath.setWordList(wordList);
            newPathList.add(newPath);
        }

        return newPathList;
    }
}
