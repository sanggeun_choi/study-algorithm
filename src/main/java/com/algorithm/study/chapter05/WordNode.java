package com.algorithm.study.chapter05;


import java.util.*;

/**
 * Created by john on 2017-01-20.
 */
public class WordNode {
    private String name;
    private Set<WordNode> adjoinWordNodeSet = new HashSet<WordNode>();
    private List<Path> pathList = new ArrayList<Path>();
    private boolean isVisited;

    public WordNode(String name) {
        this.name = name;
        pathList.add(new Path());
    }

    public void addAdjoinWordNode(WordNode wordNode) {
        adjoinWordNodeSet.add(wordNode);
    }

    public void addPath(String word) {
        for (Path path : pathList) {
            path.addPath(word);
        }
    }

    public void copyPathList(List<Path> pathList) {
        this.pathList = CopyUtils.copy(pathList);
    }

    public void addPathList(List<Path> pathList) {
        this.pathList.addAll(CopyUtils.copy(pathList));
    }

    public int getPathSize() {
        return pathList.get(0).getPathSize();
    }

    public String getName() {
        return name;
    }

    public Set<WordNode> getAdjoinWordNodeSet() {
        return adjoinWordNodeSet;
    }

    public List<Path> getPathList() {
        return pathList;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }
}
