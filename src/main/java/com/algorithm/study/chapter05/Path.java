package com.algorithm.study.chapter05;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2017-01-20.
 */
public class Path implements Comparable<Path> {
    private List<String> wordList = new ArrayList<String>();

    public void addPath(String word) {
        wordList.add(word);
    }

    public int getPathSize() {
        return wordList.size();
    }

    public List<String> getWordList() {
        return wordList;
    }

    public void setWordList(List<String> wordList) {
        this.wordList = wordList;
    }

    public int compareTo(Path o) {
        Integer size1 = wordList.size();
        Integer size2 = wordList.size();
        return size1.compareTo(size2);
    }

    @Override
    public String toString() {
        if (wordList.isEmpty()) {
            return "";
        }

        String words = "";

        for (String word : wordList) {
            words += word + " ";
        }

        return words;
    }
}
