package com.algorithm.study.chapter05;

import java.util.List;

/**
 * Created by john on 2017-01-20.
 */
public class Driver {
    public static void main(String[] args) {
        WordLadder wordLadder = new WordLadder();

        List<Path> pathList = wordLadder.searchShortestPath("hit", "cog");

        for (Path path : pathList) {
            System.out.println(path);
        }
    }
}
