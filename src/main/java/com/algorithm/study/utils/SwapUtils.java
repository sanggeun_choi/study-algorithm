package com.algorithm.study.utils;

import java.util.List;

/**
 * @author sanggeun.choi
 */
public class SwapUtils {
	public static <E extends Comparable<? super E>> void swap(List<E> list, int index1, int index2) {
		E temp = list.get(index1);
		list.set(index1, list.get(index2));
		list.set(index2, temp);
	}
}