package com.algorithm.study.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author sanggeun.choi
 */
public class IntegerListUtils {
	public static List<Integer> makeRandomIntegerList(int size) {
		Random ran = new Random();
		List<Integer> list = new ArrayList<Integer>();

		for(int i=0; i<size; i++) {
			list.add(ran.nextInt(size));
		}

		return list;
	}

	public static void printRandomIntegerList(List<Integer> randomIntegerList) {
		for (Integer randomInteger : randomIntegerList) {
			System.out.print(randomInteger + " ");
		}
		System.out.println();
	}
}