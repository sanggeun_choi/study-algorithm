package com.algorithm.study.chapter06;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2017-01-24.
 */
public class Driver {
    private static final List<String> DICTIONARY_WORD_LIST = new ArrayList<String>();
    static {
        DICTIONARY_WORD_LIST.add("hot");
        DICTIONARY_WORD_LIST.add("dot");
        DICTIONARY_WORD_LIST.add("dog");
        DICTIONARY_WORD_LIST.add("lot");
        DICTIONARY_WORD_LIST.add("log");
    }

    public static void main(String[] args) {
        List<Node> endNodeList = WordLadder.searchPath(DICTIONARY_WORD_LIST, "hit", "cog");

        int count = 0;
        for (Node node : endNodeList) {
            Node visit = node;
            System.out.print("[" + (count++) + "] : ");
            while (visit != null) {
                System.out.print(visit.getWord() + " ");
                visit = visit.getParent();
            }
            System.out.println();
        }
    }
}
