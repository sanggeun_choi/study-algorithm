package com.algorithm.study.chapter06;

import java.util.*;

/**
 * Created by john on 2017-01-24.
 */
public class WordLadder {
    public static List<Node> searchPath(List<String> dictionaryWordList, String startWord, String endWord) {
        Queue<Node> queue = new LinkedList<Node>();
        List<String> path = new ArrayList<String>();
        List<Node> rootNodeList = new ArrayList<Node>();

        dictionaryWordList.addAll(Arrays.asList(startWord, endWord));
        queue.add(new Node(startWord));

        while (!queue.isEmpty()) {
            Node currentNode = queue.remove();
            if (currentNode.getParent() != null && !path.contains(currentNode.getParent().getWord())) {
                path.add(currentNode.getParent().getWord());
            }

            for (String word : dictionaryWordList) {
                if (isOneCharacterDifferent(currentNode.getWord(), word) && !path.contains(word)) {
                    Node wordNode = new Node(word);
                    wordNode.setParent(currentNode);

                    if (word.equals(endWord)) {
                        rootNodeList.add(wordNode);
                    } else {
                        queue.add(wordNode);
                    }
                }
            }
        }

        return rootNodeList;
    }

    private static boolean isOneCharacterDifferent(String word1, String word2) {
        int count = 0;

        for (int i=0; i<word1.length(); i++) {
            char c1 = word1.charAt(i);
            char c2 = word2.charAt(i);

            if (c1 != c2) {
                count++;
            }
        }

        return count == 1;
    }
}
