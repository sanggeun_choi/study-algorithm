package com.algorithm.study.chapter06;

import lombok.Data;

/**
 * Created by john on 2017-01-24.
 */
@Data
public class Node {
    private String word;
    private Node parent;

    public Node(String word) {
        this.word = word;
    }
}
