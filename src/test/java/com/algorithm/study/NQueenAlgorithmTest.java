package com.algorithm.study;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.algorithm.study.chapter02.NQueenAlgorithm;

/**
 * Created by john on 2016-12-18.
 */
@RunWith(Parameterized.class)
public class NQueenAlgorithmTest {
    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {1, 1},
                {2, 0},
                {3, 0},
                {4, 2},
                {5, 10},
                {6, 4},
                {7, 40},
                {8, 92},
                {9, 352},
                {10, 724},
                {11, 2680},
                {12, 14200},
                {13, 73712}
        });
    }

    private int input;
    private int expected;

    public NQueenAlgorithmTest(int input, int expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    @Ignore
    public void positionQueens테스트() {
        Assert.assertEquals(expected, NQueenAlgorithm.placeQueen(input));
    }

}
